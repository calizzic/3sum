class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        """
        -Cant duplicate triplets
        -Two sum *n (fix one number and make that the target)
        """
        result = []
        added = set()
        #Cycle through list in progressively smaller segments
        for i in range(len(nums)-2):
            once = set()
            twice = set()
            #Create set of all values that happen once and twice respectively
            for j in range(i+1,len(nums)):
                if nums[j] in once:
                    twice.add(nums[j])
                else:
                    once.add(nums[j])
            #move through list again finding solutions and adding them to result        
            for j in range(i+1,len(nums)):
                value = -nums[j]-nums[i]
                if(((value in once) and value !=nums[j]) or value in twice):
                    triplet = [nums[i],nums[j],value]
                    triplet.sort()
                    added.add(tuple(triplet))
        #Convert from a set to a list
        for t in added:
            result.append(t)
        return result